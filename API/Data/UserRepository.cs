using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.DTO;
using API.Entities;
using API.Helpers;
using API.Interfaces;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;

namespace API.Data
{
    public class UserRepository : IUserRepository
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;
        public UserRepository(DataContext context, IMapper mapper)
        {
            _mapper = mapper;
            _context = context;
        }

        public async Task<MemberDto> GetMemberAsync(string username)
        {
            return await _context.User
            .Where(x => x.UserName == username)
            .ProjectTo<MemberDto>(_mapper.ConfigurationProvider)
            .SingleOrDefaultAsync();
        }

      

      /*   public  async Task<PagedList<MemberDto>> GetMembersAsync(UserParams userParams)
        {
           var query= _context.User

          //  var minDob=DateTime.Today.AddYears(-userParams.MaxAge) 

           .ProjectTo<MemberDto>(_mapper.ConfigurationProvider)
           .AsNoTracking();
        return await PagedList<MemberDto>.CreateAsync(query,userParams.PageNumber,userParams.PageSize);
        }*/


        public  async Task<PagedList<MemberDto>> GetMembersAsync(UserParams userParams)
        {
           var query= _context.User.AsQueryable();
           var minDob=DateTime.Today.AddYears(-userParams.MaxAge -1);
           var maxDob=DateTime.Today.AddYears(-userParams.MinAge -1);

            query=query.Where(u => u.UserName != userParams.CurrentUsername);
            query=query.Where(u => u.DateOfBirth >= minDob && u.DateOfBirth <= maxDob);

            query=userParams.OrderBy switch{
                "created" => query.OrderByDescending(u => u.Created),
                _ => query.OrderByDescending(u => u.LastActive)
            };

           return await PagedList<MemberDto>.CreateAsync(query.ProjectTo<MemberDto>(_mapper.ConfigurationProvider).AsNoTracking(),userParams.PageNumber,userParams.PageSize);

        }
        public async Task<AddUser> GetUserByUsernameAsync(string username)
        {
            return await _context.User
            .Include(p => p.Photos)
            .SingleOrDefaultAsync(x => x.UserName == username);
        }

        public async Task<IEnumerable<AddUser>> GetUsersAsync()
        {
            return await _context.User
            .Include(p => p.Photos)
            .ToListAsync();
        }

        public async Task<AddUser> GetUsersByIdAsync(int id)
        {
            return await _context.User.FindAsync(id);
        }

        public async Task<bool> SaveAllAsync()
        {
            return await _context.SaveChangesAsync() > 0;
        }

        public void Update(AddUser user)
        {
            _context.Entry(user).State = EntityState.Modified;
        }
    }
}