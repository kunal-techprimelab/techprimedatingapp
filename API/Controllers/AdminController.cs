using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using API.Entities;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace API.Controllers
{
    public class AdminController : BaseApiController
    {
        private readonly UserManager<AddUser> userManager;
        public AdminController(UserManager<AddUser> userManager)
        {
            this.userManager = userManager;

        }
        [Authorize(Policy = "RequiredAdminRole")]
        [HttpGet("users-with-roles")]

        public async Task<ActionResult> GetUsersWithRole()
        {
            var users=await this.userManager.Users
            .Include(r => r.UserRoles)
            .ThenInclude(r => r.Role)
            .OrderBy(u => u.UserName)
            .Select(u => new{
                u.Id,
                Username=u.UserName,
                Roles =u.UserRoles.Select(r => r.Role.Name).ToList()
            })
            .ToListAsync();

        return Ok(users);
        }

        [HttpPost("edit-roles/{username}")]
        public async Task<ActionResult> EditRoles(string username, [FromQuery] string roles)
        {
            var selectedRoles = roles.Split(",").ToArray();

            var user= await this.userManager.FindByNameAsync(username); 

            if (user == null) return NotFound("Could not find User");

            var userRoles= await this.userManager.GetRolesAsync(user);

            var result=await this.userManager.AddToRolesAsync(user,selectedRoles.Except(userRoles));

            if (!result.Succeeded) return BadRequest("Failed to add to roles");

            result=await this.userManager.RemoveFromRolesAsync(user, userRoles.Except(selectedRoles));

             if (!result.Succeeded) return BadRequest("Failed to remove from roles");

             return Ok(await this.userManager.GetRolesAsync(user));
        }

        [Authorize(Policy = "ModeratePhotoRole")]
        [HttpGet("photos-to-moderate")]

        public ActionResult GetPhotosForModeration()
        {
            return Ok("Only admins or moderators can see this");
        }
    }
}