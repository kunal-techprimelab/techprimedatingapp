using Microsoft.AspNetCore.Identity;

namespace API.Entities
{
    public class AppUserRole : IdentityUserRole<int>
    {
        public AddUser Userr {get ; set ;}

        public AppRole Role { get; set; }
    }
}