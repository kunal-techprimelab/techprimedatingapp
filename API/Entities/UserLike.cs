namespace API.Entities
{
    public class UserLike
    {
        public AddUser SourceUser{ get; set; }

        public int SourceUserId { get; set; }

      public AddUser LikedUser{ get; set; }

        public int LikedUserId { get; set; }


    }
}