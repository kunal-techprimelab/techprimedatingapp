using System.Collections.Generic;
using System.Threading.Tasks;
using API.DTO;
using API.Entities;
using API.Helpers;

namespace API.Interfaces
{
    public interface IUserRepository
    {
        void Update(AddUser user);
        Task<bool> SaveAllAsync();
        Task<IEnumerable<AddUser>> GetUsersAsync();
        Task<AddUser> GetUsersByIdAsync(int id);
        Task<AddUser> GetUserByUsernameAsync(string username);

        Task<PagedList<MemberDto>> GetMembersAsync(UserParams userParams); 
        Task<MemberDto> GetMemberAsync(string username); 

    }
}